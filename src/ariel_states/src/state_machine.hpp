#pragma once

#include <boost/statechart/state_machine.hpp>
#include <boost/statechart/simple_state.hpp>
#include <boost/statechart/state.hpp>
#include <boost/statechart/transition.hpp>
#include <boost/statechart/custom_reaction.hpp>
#include <boost/mpl/list.hpp>

#include <string>
#include <iostream>

#include "fibo.hpp"


namespace sc = boost::statechart;


/**
 * @brief Drone's state machine
 * Drone's state machine with all of its States and 
 * Events declared.
 */
class ArielStateMachine
{

public:

    ArielStateMachine();
    ~ArielStateMachine();


private:

    /**
     * @brief Pointer to all action clients.
     * Client is passed to every state as context, therefore 
     * every action client needed will be provided through
     * this struct to the States.
     */
    struct Client
    {
        std::shared_ptr<MinimalActionClient> action_client = std::make_shared<MinimalActionClient>();
    };


    /**
     * @brief The State Machine States
     */
    class States
    {
    public:
        struct StandBy;
        struct GoToBoat;
        struct DetectAruco;
        struct CorrectPosition;
        struct Landing;
    };


    /**
     * @brief The State Machine Events
     */
    class Events
    {
    public:
        // Rename later
        struct E1 : sc::event< E1 > {};
        struct E2 : sc::event< E2 > {};
        struct E3 : sc::event< E3 > {};
        struct E4 : sc::event< E4 > {};
        struct E5 : sc::event< E5 > {};
    };


    // Initiate machine and set StandBy as the first state
    struct StateMachine : sc::state_machine< StateMachine, States::StandBy > {};

    StateMachine sm;

};


using Ariel = ArielStateMachine;


ArielStateMachine::ArielStateMachine() 
{
    sm.initiate();
}

ArielStateMachine::~ArielStateMachine(){}


struct Ariel::States::StandBy : Ariel::Client, sc::state<Ariel::States::StandBy, Ariel::StateMachine>
{
    typedef sc::transition<Ariel::Events::E1, Ariel::States::GoToBoat> reactions;

    StandBy(my_context ctx ) : my_base( ctx )
    {
        std::cout << "Entering StandBy" << std::endl;
        action_client->set_goal(1);
        action_client->send_goal();

        while (!action_client->is_goal_done()) {
            rclcpp::spin_some(action_client);
        }

        post_event(Ariel::Events::E1());
    }
    ~StandBy() {std::cout << "Exiting StandBy" << std::endl;}
};


struct Ariel::States::GoToBoat : Ariel::Client, sc::state<Ariel::States::GoToBoat, Ariel::StateMachine>
{
    typedef sc::transition<Ariel::Events::E2, Ariel::States::DetectAruco> reactions;
    GoToBoat(my_context ctx ) : my_base( ctx ) 
    {
        std::cout << "Entering GoToBoat\n";

        action_client->set_goal(2);
        action_client->send_goal();

        while (!action_client->is_goal_done()) {
            rclcpp::spin_some(action_client);
        }

        post_event(Ariel::Events::E2());
    }
    ~GoToBoat() {std::cout << "Exiting GoToBoat" << std::endl;}
};


struct Ariel::States::DetectAruco : Ariel::Client, sc::state<Ariel::States::DetectAruco, Ariel::StateMachine>
{
    typedef sc::transition<Ariel::Events::E3, Ariel::States::CorrectPosition> reactions;
    DetectAruco(my_context ctx ) : my_base( ctx ) 
    {
        std::cout << "Entering GoToBoat\n";

        action_client->set_goal(3);
        action_client->send_goal();

        while (!action_client->is_goal_done()) {
            rclcpp::spin_some(action_client);
        }

        post_event(Ariel::Events::E3());
    }
    ~DetectAruco() {std::cout << "Exiting DetectAruco" << std::endl;}
};


struct Ariel::States::CorrectPosition : sc::state<Ariel::States::CorrectPosition, Ariel::StateMachine>
{
    typedef sc::transition<Ariel::Events::E4, Ariel::States::Landing> reactions;
    CorrectPosition(my_context ctx ) : my_base( ctx )
    {
        std::cout << "Entering CorrectPosition\n";
        post_event(Ariel::Events::E4());
    }
    ~CorrectPosition() {std::cout << "Exiting CorrectPosition" << std::endl;}
};


struct Ariel::States::Landing : sc::state<Ariel::States::Landing, Ariel::StateMachine>
{
    typedef sc::transition<Ariel::Events::E5, Ariel::States::StandBy> reactions;
    Landing(my_context ctx ) : my_base( ctx )
    {
        std::cout << "Entering Landing\n";
        //post_event(AEvent::E5());
    }
    ~Landing() {std::cout << "Exiting Landing" << std::endl;}
};


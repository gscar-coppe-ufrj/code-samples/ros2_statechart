#include <chrono>
#include <thread>
#include <iostream>

#include "rclcpp/rclcpp.hpp"

#include "state_machine.hpp"
#include "fibo.hpp"


int main(int argc, char** argv)
{
    rclcpp::init(argc, argv);

    ArielStateMachine aStateMachine;

    rclcpp::shutdown();

    return 0;
}
